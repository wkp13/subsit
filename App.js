import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { enableScreens } from 'react-native-screens';
enableScreens();

import { AppLoading } from 'expo';
import {
  useFonts,
  PlayfairDisplay_400Regular,
  PlayfairDisplay_500Medium,
  PlayfairDisplay_600SemiBold,
  PlayfairDisplay_700Bold,
  PlayfairDisplay_800ExtraBold,
  PlayfairDisplay_900Black,
  PlayfairDisplay_400Regular_Italic,
  PlayfairDisplay_500Medium_Italic,
  PlayfairDisplay_600SemiBold_Italic,
  PlayfairDisplay_700Bold_Italic,
  PlayfairDisplay_800ExtraBold_Italic,
  PlayfairDisplay_900Black_Italic,
} from '@expo-google-fonts/playfair-display';

import Login from './src/pages/Login'
import Register from './src/pages/Register'
import Splash from './src/pages/splash'
import Dashboard from './src/pages/Dashboard'
import Profile from './src/pages/Profile'
import OnGOing from './src/pages/OnGoing'
import History from './src/pages/OnGoing/History'
import AddSubs from './src/pages/AddSubs'
import Cards from './src/pages/Cards'
import Report from './src/pages/Report'

const Tab = createBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Dashboard"
      tabBarOptions={{
        activeTintColor: '#8F48EA',
        keyboardHidesTabBar: true
      }}
    >
      <Tab.Screen
        name="Dashboard"
        component={Dashboard}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="OnGoing"
        component={OnGOing}
        options={{
          tabBarLabel: 'On going',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="calendar-month-outline" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Report"
        component={Report}
        options={{
          tabBarLabel: 'Report',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="chart-bell-curve-cumulative" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account-outline" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
const Stack = createStackNavigator()

export default App = ()=> {
  let [fontsLoaded] = useFonts({
    PlayfairDisplay_400Regular,
    PlayfairDisplay_500Medium,
    PlayfairDisplay_600SemiBold,
    PlayfairDisplay_700Bold,
    PlayfairDisplay_800ExtraBold,
    PlayfairDisplay_900Black,
    PlayfairDisplay_400Regular_Italic,
    PlayfairDisplay_500Medium_Italic,
    PlayfairDisplay_600SemiBold_Italic,
    PlayfairDisplay_700Bold_Italic,
    PlayfairDisplay_800ExtraBold_Italic,
    PlayfairDisplay_900Black_Italic,
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  } else {

 return (
    <NavigationContainer>
      <Stack.Navigator
      screenOptions={{
        cardStyle: { backgroundColor: '#E2E2E2' },
        cardOverlayEnabled: true,
        cardStyleInterpolator: ({ current: { progress } }) => ({
          cardStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 0.5, 0.9, 1],
              outputRange: [0, 0.25, 0.7, 1],
            }),
          },
          overlayStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 1],
              outputRange: [0, 0.5],
              extrapolate: 'clamp',
            }),
          },
        }),
      }}
      mode="modal"
      headerMode="none"
      initialRouteName="Splash"
      >
        <Stack.Screen name="Splash" component={Splash} tabBarVisible={false}/>
        <Stack.Screen name="Login" component={Login} tabBarVisible={false}/>
        <Stack.Screen name="Register" component={Register} tabBarVisible={false}/>
        <Stack.Screen name="History" component={History} tabBarVisible={false}/>
        <Stack.Screen name="AddSubs" component={AddSubs} tabBarVisible={false}/>
        <Stack.Screen name="Dashboard" component={MyTabs} />
        <Stack.Screen name="Cards" component={Cards} />

      </Stack.Navigator>
    </NavigationContainer>
  );
}
}
