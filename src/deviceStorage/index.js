import AsyncStorage from '@react-native-community/async-storage'


const deviceStorage = {

    async saveKey(key, value) {
        try {
          await AsyncStorage.setItem(key, value);
        } catch (error) {
          console.log('AsyncStorage Error: ' + error.message);
        }
      },
    async getKey(token) {
        try {
         let value = await AsyncStorage.getItem(token);
         if (value !== null) {
         
          return value;
         }
        }
        catch (error) {
        
        console.log('AsyncStorage Error: ' + error.message);
        }
    },
    async removeKey(token){
      try{
        await AsyncStorage.removeItem(token)
        if(value !== null) {
          return value
        }
      } catch(error){
        console.log('AsyncStorage Error: ' + error.message);
      }
    }
        


    };

export default deviceStorage;