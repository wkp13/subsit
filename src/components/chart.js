import React from 'react'
import { StyleSheet, Text, View, Dimensions } from 'react-native'
import { LineChart } from "react-native-chart-kit"

const {height, width} = Dimensions.get('window');

export default function chart({data}) {

    return (
        <View style={styles.container}>
            <LineChart
                data={data}
                width={width / 1.15}
                height={height / 3}
                yAxisLabel="Rp"
                yAxisSuffix=""
                yAxisInterval={1}
                chartConfig={{
                backgroundColor: "#fff",
                backgroundGradientFrom: "#fff",
                backgroundGradientTo: "#fff",
                decimalPlaces: 0,
                color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                strokeWidth: 8,
                labelColor: (opacity = 1) => `rgba(20, 0, 20, ${opacity})`,
                style: {
                    borderRadius: 15,
                },
                propsForDots: {
                    r: "6",
                    strokeWidth: "5",
                    stroke: "#fdf4"
                }
                }}
                
                style={{
                marginVertical: 10,
                marginHorizontal: 5,
                borderRadius: 20,
                }}
            />
            </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})
