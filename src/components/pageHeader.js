import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Dimensions } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {  } from 'react-native-gesture-handler';

const {height, width} = Dimensions.get('window');

export default function pageHeader({
    title,
    nameLeft,
    nameRight,
    onPress,
}) {
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={onPress}>
                <MaterialCommunityIcons name={nameLeft} color='black' size={20} />
            </TouchableOpacity>
            <Text style={styles.pageTitle}>{title}</Text>
            <TouchableOpacity onPress={onPress}>
                <MaterialCommunityIcons name={nameRight} color='black' size={20} />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: 'space-between',
        width: width,
        paddingTop: 30,
        paddingBottom: 10,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity:  0.4,
        shadowRadius: 3,
        elevation: 5,
    },
    pageTitle: {
        fontFamily: 'PlayfairDisplay_700Bold',
        color: 'black',
        textAlign: 'center',
        fontSize: 20,
    }
})
