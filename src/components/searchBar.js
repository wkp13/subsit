import React from 'react'
import { Text, View, TextInput, TouchableOpacity, Dimensions } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const width = Dimensions.get('window').width


const Button = ({
    placeholder,
    onChangeText,
    type = 'light',
    bordered = false,
    size = 'small'
}) => {
  const large = width / 1.3
  const small = width / 2.5
  const srcSize = size === 'large' ? large : small
  const srcBgColor = type === 'light' ? '#fff' : 'black'
  const srcTextColor = type === 'light' ? 'black' : '#fff'
  const srcIconColor = type === 'light' ? 'black' : '#fff'
  const srcIconSize = size === 'small' ? 20 : 40
  const srcBorderRadius = bordered ? 30 : 5

  const containerCommonStyle = {
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    backgroundColor: srcBgColor,
    marginVertical: 2,
    paddingVertical: 2,
    paddingHorizontal: 5,
    width: srcSize,
    borderRadius: srcBorderRadius
  }

  const textCommonStyle = {
    color: srcTextColor,
    fontSize: 10,
    width: width / 3.5,
    textAlign: 'left',
    fontFamily: 'PlayfairDisplay_400Regular',
  }

  const border = type === 'outlined' && { borderColor: '#e7e7e7', borderWidth: 2 }


  return (
    <View>
      <View style={[containerCommonStyle, border]}>
            <MaterialCommunityIcons style={{margin: 3,}} name="magnify" color={srcIconColor} size={srcIconSize} />
          <TextInput style={textCommonStyle} placeholder={placeholder} onChangeText={onChangeText}/>
      </View>
    </View>
  )
}

export default Button