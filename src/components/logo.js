import React from 'react'
import { Text, View, TouchableOpacity, Dimensions } from 'react-native'

const width = Dimensions.get('window').width


const Logo = ({type, size}) => {
  const large = 40
  const small = 18
  const LogoSize = size === 'large' ? large : small
  const LogoColor = type === 'black' ? '#000' : '#fff'


  const LogoStyle = {
    color: LogoColor,
    fontSize: LogoSize,
    textAlign: 'center',
    fontFamily: 'PlayfairDisplay_700Bold',
  }

  return (
      <View>
        <Text style={[LogoStyle]}> SubsIt. </Text>
      </View>

  )
}

export default Logo