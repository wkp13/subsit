import React from 'react'
import { 
SafeAreaView,
  View,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  Text,
  Dimensions,
 } from 'react-native'
import moment from 'moment'
const {height, width} = Dimensions.get('window');

export default function transactionHistory({data}) {

const [selected, setSelected] = React.useState(new Map());

  const onSelect = React.useCallback(
    id => {
      const newSelected = new Map(selected);
      newSelected.set(id, !selected.get(id));

      setSelected(newSelected);
    },
    [selected],
  );

    function Item({ id, title, repetition, price, paydate, selected, onSelect }) {
        return (
          <TouchableOpacity
            onPress={() => onSelect(id)}
            style={[
              styles.item,
              { backgroundColor: selected ? '#fdf4' : 'none' },
            ]}
          >
          <View style={{
              borderBottomColor: 'black',
              borderBottomWidth: 1,
              width: width,
              paddingVertical: 15,
          }}>
              <Text style={styles.title}>{title}</Text>
              <Text style={styles.subTitle}>{repetition}</Text>
              <Text style={styles.subTitle}>Rp {price}</Text>
              <Text style={styles.subTitle}>{moment(paydate).format('MMMM Do YYYY, h:mm:ss a')}</Text>
            
            </View>
          </TouchableOpacity>
        );
      }

    return (
        <SafeAreaView>
            <FlatList
                showsVerticalScrollIndicator={false}
                data={data}
                renderItem={({ item }) => (
                <Item
                    id={item.id}
                    title={item.title}
                    repetition={item.repetition}
                    price={item.price}
                    paydate={item.paydate}
 
                    selected={!!selected.get(item.id)}
                    onSelect={onSelect}
                />
                )}
                keyExtractor={item => item.id}
                extraData={selected}
            />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({

    item: {
        justifyContent: "center",
        alignItems: "flex-start",
        },
      title: {
        fontFamily: 'PlayfairDisplay_700Bold',
        fontSize: 20,
        marginHorizontal: 5,
        marginBottom: 10,

      },
      subTitle: {
        fontSize: 12,
        marginHorizontal: 5,

      },
})
