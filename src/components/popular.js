import React, { useState, useEffect, } from 'react';
import {
  SafeAreaView,
  View,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  ListView,
  Platform,
  ToastAndroid
} from 'react-native';

import DateTimePicker from '@react-native-community/datetimepicker'
import Modal from 'react-native-modal'
import Button from './button';
import moment from 'moment'
import deviceStorage from '../deviceStorage/index'
import axios from 'axios'
import DropDownPicker from 'react-native-dropdown-picker'

const {height, width} = Dimensions.get('window');

export default function Popular({data}) {
  
  const [isModalSelected, setisModalSelected] = useState(false)
  const [image_url, setimage_url] = useState('')
  const [title, settitle] = useState('')
  const [description, setdescription] = useState('')
  const [id, setid] = useState('')
  const [service_id, setservice_id] = useState('')
  const [service_type, setservice_type] = useState([])
  const [service_type_id, setservice_type_id] = useState('')
  const [serviceTypeSelected, setserviceTypeSelected] = useState(false)

  const [service_name, setservice_name] = useState('')
  const [service_repetition, setservice_repetition] = useState('')
  const [service_price, setservice_price] = useState('')
  const [next_step_modal, setnext_step_modal] = useState(false)

  const [cardsData, setcardsData] = useState([])
  const [card_id, setcard_id] = useState('')
  const [cardSelected, setcardSelected] = useState(false)

  console.log('id', id)
  console.log('service_id',service_id)
  console.log('service_type_id',service_type_id)
  


  async function subscribe () {
    
    const key = await deviceStorage.getKey('@token')
    
      let service_id = id
      let starting_date = moment(date).format('DD/MM/YYYY')
      try {
        await axios.post(
          'https://subsit-team-a.herokuapp.com/api/v1/subscribe',
          {
              service_id,
              service_type_id,
              card_id,
              starting_date,
              
          },
          {
              headers: {
                  Authorization: key,
              }
          })
        }catch(error) {
            console.log('get popular: ', error)
          }
  }

  

  let type = service_type.map((dataService) => {
    dataService.status = false
    return dataService
  })

  function selected(item) {
    type = type.map((selectedItem) => {
      if(selectedItem.id == item.id) {
        selectedItem.status = true
      }
      if(selectedItem.status == true && selectedItem.id != item.id) {
        selectedItem.status = false
      }
      return selectedItem
    })
  }

  console.log('dataaa :',  data)
  console.log('service type', service_type)
  
  


  const [date, setDate] = useState(new Date(1598051730000))
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios' ? true : false);
    setDate(currentDate);
  };

  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const getCards = async () => {

    const key = await deviceStorage.getKey('@token')
    
    
    try {
      const res = await axios.get(
          'https://subsit-team-a.herokuapp.com/api/v1/user/card',
          {
  
          headers: {
                Authorization: key,
              }
          })
      
      if (res !== null) {
          const apiRes = res.data.data.Card;
          setcardsData(apiRes)
          
      }
  }catch(error) {
      console.log('get card: ', error)
    }
  }
  useEffect(() => {
      getCards()
  }, [])


  function CardItem({source, card_number, id, cardSelected}) {
    return(
    <View style={{
      marginHorizontal: 8,
      marginBottom: 5,
      backgroundColor: cardSelected? '#6e3b6e' : '#fff',
      borderRadius: 3,
      }}>
        <TouchableOpacity onPress={()=> {
            setcard_id(id)
            setcardSelected(true)
            }}>
            <Text style={{
                marginTop: 6,
                fontSize: 9,
                color: cardSelected? '#fff' : 'black',
                }}>{source}</Text>
            <Text style={{
                marginTop: 3,
                fontSize: 7,
                color: cardSelected? '#fff' : 'black',
            }}>{card_number}</Text>
        </TouchableOpacity>
        
    </View>
    )
}
console.log('cards data', cardsData)
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={data}
        renderItem={({ item }) => (

          <TouchableOpacity
            onPress={() => {
              setisModalSelected(true)
              setnext_step_modal(false)
              setimage_url(item.image_url)
              settitle(item.title)
              setdescription(item.description)
              setid(item.id)
              setservice_type(item.type)
              console.log('item.type',item.type)
              setShow(false)
            }}
            style={[
              styles.item,
              { backgroundColor: '#fff'},
            ]}
          >
            <Image
              source={{ uri: `${item.image_url}` }}
              style={styles.tinyLogo}
            />
            <Text style={styles.title}>{item.title}</Text>
        </TouchableOpacity>
        )}
        keyExtractor={item => item.id}
      />
        <Modal
        isVisible={isModalSelected}
        onBackdropPress={() => setisModalSelected(false)}
        animationIn='flipInX'
        useNativeDriver
        onBackButtonPress={() => {
          setisModalSelected(false)
          setnext_step_modal(false)
          setservice_name('')
          setservice_repetition('')
          setservice_id(item.id)
          setservice_price('')
        }}
        >
          <View style={{
            height: height / 2,
            width: width/1.12,
            backgroundColor: '#fff',
            justifyContent: "space-between",
            paddingHorizontal: 15,
            borderRadius: 8,
            }}>
           {next_step_modal? 
            <View>
              <Text style={styles.titleExtraBig}>Pick Details.</Text>
              <View style={{marginVertical: 10, borderWidth: 1, borderColor: '#6e3b6e', borderRadius: 5,}}>
              
                <Text style={styles.titleBig}>{service_name}</Text>
                <Text style={styles.subTitle}>{service_repetition}</Text>
                <Text style={styles.subTitle}>Rp. {service_price}</Text>
                <TouchableOpacity onPress={showDatepicker} style={{paddingVertical: 10, backgroundColor: '#6e3b6e'}}>
                  <Text style={{fontSize: 16, paddingLeft: 5, color: '#fff'}}>Starting at {moment(date).format('DD-MM-YYYY')}</Text>
                  {show && (
                    <DateTimePicker
                      testID="dateTimePicker"
                      value={date}
                      mode={mode}
                      is24Hour={true}
                      display="default"
                      onChange={onChange}
                    />
                  )}
                </TouchableOpacity>
                
              </View>
              <View>
                <TouchableOpacity style={{borderColor: '#6e3b6e', borderWidth: 1, borderRadius: 5}}>
                  <Text style={styles.titleBig}>Your payment details</Text>
            
                 <FlatList
                 data={cardsData}
                 showsHorizontalScrollIndicator={false}
                 horizontal
                 renderItem={({ item }) => (
                      <CardItem
                      source={item.source}
                      card_number={item.card_number}
                      id={item.id}
                      cardSelected={cardSelected}
                      keyExtractor={item => item.id}
                      />
                      )}

                 />
                  
                </TouchableOpacity>
              </View>
              <View style={{justifyContent: 'center', alignSelf: 'center', marginBottom: 10,}}>
                <Button size='small' bordered text='Cancel'
                onPress={()=> {
                  setnext_step_modal(false)
                  setShow(false)
                  setservice_name('')
                  setservice_repetition('')
                  setservice_price('')
                  }}
                />
                <Button size='small' onPress={()=> {
                  ToastAndroid.show(`Subcribed to ${title} ${service_name}`, ToastAndroid.SHORT)
                  setisModalSelected(false)
                  setShow(false)
                  subscribe()
                }} 
                bordered text='Add to subscription'/> 
              </View>

            
            </View>
            :
            <View>
              <Image
              source={{ uri: `${image_url}` }}
              style={styles.bigLogo}
            />
            <Text style={styles.titleBig}>{title}</Text>
            <Text style={{
            padding: 5,  
            }}>
            {description}
            </Text>
            <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal
            data={type}
            renderItem={({ item }) => (
              
              <TouchableOpacity
                onPress={() => {
                  setservice_type_id(item.id)
                  
                  setservice_name(item.name)
                  setservice_repetition(item.repetition)
                  setservice_price(item.price)
                  setnext_step_modal(true)
                  setShow(false)
                  setserviceTypeSelected(true)
                  selected(item)
                  console.log('status ', item.status)
                  console.log ('type : ', type)
                }}
                
              >
                <View style={[
                  styles.serviceType,
                  {backgroundColor: item.status? '#6e3b6e' : '#fff'},
                ]}>
                <Text style={{ fontFamily: 'PlayfairDisplay_700Bold', fontSize: 15, marginBottom: 10,}}>{item.name}</Text>
                <Text style={styles.serviceTypeName}>{item.repetition}</Text>
                <Text style={styles.serviceTypeName}>Rp {item.price}</Text>
                </View>
            </TouchableOpacity>
            )}
            keyExtractor={item => item.id}
            extraData={type}
            />
            </View>
            }
            
          </View>
        </Modal>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
  },
  item: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: 'row',
    backgroundColor: 'white',
    marginVertical: 10,
    marginHorizontal: 5,
    padding: 10,
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: {
	width: 0,
	height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,

    elevation: 2,
    },
  title : {
    color: 'black',
    fontFamily: 'PlayfairDisplay_500Medium',
    marginHorizontal: 10,
  },
  tinyLogo: {
      width: 40,
      height: 40,
      borderWidth: 1,
  },
  bigLogo : {
    width: width/1.12,
    height: 120,
    alignSelf: "center",
    borderRadius: 8,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
  },
  titleBig : {
    color: 'black',
    fontFamily: 'PlayfairDisplay_700Bold',
    marginVertical: 10,
    paddingLeft: 5, 
    fontSize: 18,
  },
  titleExtraBig : {
    color: 'black',
    fontFamily: 'PlayfairDisplay_700Bold',
    marginVertical: 10,
    paddingLeft: 5, 
    fontSize: 24,
  },
  subTitle : {
    paddingLeft: 5, 
  },
  serviceType: {
    padding: 5,
    margin: 5,
    width: 120,
    flexDirection: 'column',
    borderColor: '#6e3b6e',
    borderRadius: 5,
    borderWidth: 1,
  },
  serviceTypeName: {
    fontSize: 11,
    marginRight: 15,
  }
});
