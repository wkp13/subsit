import React from 'react';
import {
  SafeAreaView,
  View,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  Text,
  Image,
} from 'react-native';
import Constants from 'expo-constants';
import moment from 'moment'

function Item({ id, title, next_paid_date, price, image_url, selected, onSelect }) {
  return (
    <TouchableOpacity
      onPress={() => onSelect(id)}
      style={[
        styles.item,
        { backgroundColor: selected ? '#6e3b6e' : '#fff' },
      ]}
    >
      <Image
        source={{ uri: `${image_url}` }}
        style={styles.tinyLogo}
      />
      <View>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.subTitle}>{moment(next_paid_date).format('MMMM Do YYYY')} </Text>
        <Text style={styles.subTitle}>Rp {price}</Text>
      </View>
    </TouchableOpacity>
  );
}

export default function Upcoming({data}) {
  const [selected, setSelected] = React.useState(new Map());

  const onSelect = React.useCallback(
    id => {
      const newSelected = new Map(selected);
      newSelected.set(id, !selected.get(id));

      setSelected(newSelected);
    },
    [selected],
  );

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={data}
        renderItem={({ item }) => (
          <Item
            id={item.id}
            title={item.title}
            next_paid_date={item.next_paid_date}
            price={item.price}
            image_url={item.image_url}
            selected={!!selected.get(item.id)}
            onSelect={onSelect}
          />
        )}
        keyExtractor={item => item.id}
        extraData={selected}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
  },
  item: {
    alignItems: "center",
    flexDirection: 'row',
    backgroundColor: 'white',
    padding: 5,
    marginVertical: 10,
    marginHorizontal: 5,
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: {
    width: 2,
    height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,

    elevation: 2,
    },
  title: {
    fontSize: 11,
    marginHorizontal: 5,
    fontFamily: 'PlayfairDisplay_700Bold',
  },
  subTitle: {
    fontSize: 9,
    marginHorizontal: 5,
    paddingRight: 2,
  },
  tinyLogo: {
      width: 40,
      height: 40,
  }
});
