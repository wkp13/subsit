import React from 'react'
import { StyleSheet, Text, View,TextInput } from 'react-native'

export default function InputField({
    placeholder,
    onChangeText,
    secureTextEntry,
}) {
    return (
        <View style={styles.InputField}>
            <TextInput
            onChangeText={onChangeText}
            placeholder={placeholder}
            secureTextEntry={secureTextEntry}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    InputField: {
        justifyContent: 'space-between',
        borderBottomWidth: 0.7,
        borderBottomColor: 'black',
        marginBottom: 20,
    },
    TextInput: {
        paddingVertical: 3,
    }
})
