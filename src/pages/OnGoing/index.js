import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View, Dimensions, SafeAreaView, FlatList } from 'react-native'
import Header from '../../components/pageHeader'
import {Calendar, CalendarList} from 'react-native-calendars'
import moment from 'moment'
import deviceStorage from '../../deviceStorage'
import axios from 'axios'



const {height, width} = Dimensions.get('window');

export default function OnGoing({navigation}) {

  const [calendarData, setCalendarData] = useState([])

    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();
    
    console.log('year',year)
    console.log('month',month)

    const getCalendarData = async () => {

      const key = await deviceStorage.getKey('@token')

      
      try {
        const res = await axios.get(
            `https://subsit-team-a.herokuapp.com/api/v1/user/transactions/upcoming/${year}/${month}`,
            {
    
            headers: {
                  Authorization: key,
                }
            })
        
        if (res !== null) {
            const apiRes = res.data.data;
            setCalendarData(apiRes)
            console.log('api result', apiRes)

            apiRes.forEach((data) => {

              console.log('data dari foreach', data)
                let dateColection = []
                
                if(data.upcoming_date.length>-1){
                  data.upcoming_date.forEach((day) => {
                    console.log('day',day)
                    dateColection.push(moment(day).format('YYYY-MM-DD'))
                  })
                }
                console.log('date colection: ', dateColection)

                let newDaysObject = {}

                    dateColection.forEach((day) => {
                    newDaysObject[day] = {
                        selected: true,
                        marked: true,
                        selectedColor: 'black',
                    }
                })
                setNewDaysObject(newDaysObject)
                console.log('newdays obj:', newDaysObject)
                
                

                let result = [...apiRes
                  .reduce((r,{upcoming_date, title}) => (
                    upcoming_date.forEach((s,_,__,date=s.slice(0,10)) =>
                        r.set(
                          date, 
                          {date, title: [...(r.get(date)?.title||[]), title]}
                        )),r),
                    new Map())
                  .values()
                ]
                setBottomDate(result)
                console.log('result', result)
              })
        }
        }catch(error) {
          console.log('get popular: ', error)
        }
    }

    useEffect( async () => {

      let calendarApi = await getCalendarData()
      console.log('calendar api: ', calendarApi)
      return () => {}

  }, [])
    

    const [newDaysObject, setNewDaysObject] = useState({})
    const [upcomingDate, setUpcomingDate] = useState([])
    const [botomDate, setBottomDate] = useState([])

    console.log('botomDate: ', botomDate)
    console.log('new days object',newDaysObject)

    function Item({date, title})  {
        
      
        
        return(
        <View style={{flexDirection: 'row', marginTop: 15,}}>
            <View style={{marginHorizontal: 10, borderRadius:3, backgroundColor: 'black', justifyContent: 'center'}}>
                <Text style={{color: 'white', padding:7,}}>{moment(date).format('DD')}</Text>
            </View>
            <View style={{flexDirection: 'column'}}>
                {title.map((t,i)=> 
                            <View style={{flexDirection: 'column'}}>
                              <Text style={{
                                borderBottomColor: 'black',
                                borderBottomWidth:1,
                                }}>{t} </Text>
                              <Text>{i != title.length? '\n' : ' '}</Text>
                            </View>
                         )}
            </View>
        </View>

        )
    }
    return (
        <View style={styles.container}>
            <Header title='On Going Subsciptions' nameRight='history' onPress={()=> navigation.navigate('History')}/>
            
            <View style={{marginTop: 60,}}>
            <Calendar markedDates={newDaysObject}/>
            </View>
            <FlatList
            data={botomDate}
            renderItem={({ item }) => (
                <Item
                  
                  title={item.title}
                  date={item.date}
                  keyExtractor={(item, index) => index.toString()}
                />
              )}
            />
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#fff',
    }
})
