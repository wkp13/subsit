import React, { useState, useEffect, } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Header from '../../components/pageHeader'
import TansactionHistory from '../../components/transactionHistory'
import deviceStorage from '../../deviceStorage'
import axios from 'axios'

export default function History({navigation}) {

    const [historyData, sethistoryData] = useState([])
    console.log('HISTORY DATA',historyData)
    const getTansaction = async () => {
    const key = await deviceStorage.getKey('@token')
      console.log(key)
      
      try {
        const res = await axios.get(
            'https://subsit-team-a.herokuapp.com/api/v1/user/transactions',
            {
    
            headers: {
                  Authorization: key,
                }
            })
        
        if (res !== null) {
            const apiRes = res.data.data;
            sethistoryData(apiRes)
        }
    }catch(error) {
        console.log('get popular: ', error)
      }
    
    }
    useEffect(() => {
      getTansaction()
    }, [])
    
    

    return (
        <View style={styles.container}>
            <Header title='Transaction History' nameLeft='arrow-left' onPress={()=> navigation.navigate('OnGoing')}/>
            <View style={{marginTop: 60}}>
            <TansactionHistory data={historyData}/>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "flex-start",
        backgroundColor: '#fff',
        padding: 20,
    }
})
