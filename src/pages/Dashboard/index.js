import React, { useState, useEffect } from 'react'
import { StyleSheet,
  Text,
  View,
  Dimensions,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  ToastAndroid
} from 'react-native'
import axios from 'axios'
import deviceStorage from '../../deviceStorage'
import Chart from '../../components/chart'
import Upcoming from '../../components/upcoming'
import Popular from '../../components/popular'
import Search from '../../components/searchBar'
import Logo from '../../components/logo'
import Background from '../../assets/background.svg'
import Button from '../../components/button'
import Modal from 'react-native-modal'
import DateTimePicker from '@react-native-community/datetimepicker'
import InputField from '../../components/inputField'
import moment from 'moment'

const {height, width} = Dimensions.get('window');

export default function Dashboard({navigation}) {

  const [popularData, setPopularData] = useState([])
  const [upcomingData, setupcomingData] = useState([])
  const [chartData, setchartData] = useState({})
  const [showAddCustom, setshowAddCustom] = useState(false)


  const [cardsData, setcardsData] = useState([])
  const [card_id, setcard_id] = useState('')
  const [cardSelected, setcardSelected] = useState(false)

  const [date, setDate] = useState(new Date(1598051730000))
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

  let starting_date = date
  const [title, settitle] = useState('')
  const [description, setdescription] = useState('')
  const [name, setname] = useState(name)
  const [price, setprice] = useState(10000)
  const [repetition, setrepetition] = useState('')

  const handleTitle = (val) => {
      settitle(val)
  }
  const handleDescription = (val) => {
    setdescription(val)
  }
  const handleName = (val) => {
    setname(val)
  }
  const handlePrice= (val) => {
    setprice(val)
  }
  const handleRepetition = (val) => {
    setrepetition(val)
  }

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios' ? true : false);
    setDate(currentDate);
  };

  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };


  var month = new Date().getMonth() + 1;
  var year = new Date().getFullYear();

    const GetPopular = async () => {

      const key = await deviceStorage.getKey('@token')
      console.log(key)
      
      try {
        const res = await axios.get(
            'https://subsit-team-a.herokuapp.com/api/v1/service',
            {
    
            headers: {
                  Authorization: key,
                }
            })
        
        if (res !== null) {
            const apiRes = res.data.data;
            setPopularData(apiRes)
        }
    }catch(error) {
        console.log('get popular: ', error)
      }
    }

    useEffect(async () => {
      await GetPopular()

        await getUpcoming()

      
      await getChartData()
    }, [])

    const sepentData = {
        labels: ["May", "Jun", "Jul",],
        datasets: [
          {
            data: [
              49000,
              128000,
              172000,
            ]
          }
        ]
      }

      console.log('spent data', sepentData)

      const getUpcoming = async () => {

        const key = await deviceStorage.getKey('@token')
        console.log(key)
        
        try {
          const res = await axios.get(
              `https://subsit-team-a.herokuapp.com/api/v1/user/transactions/upcoming/${year}/${month}`,
              {
      
              headers: {
                    Authorization: key,
                  }
              })
          
          if (res !== null) {
              const apiRes = res.data.data;
              setupcomingData(apiRes)
              console.log('api result upcoming', apiRes)
            }
          }catch(error) {
            console.log('get upcoming: ', error)
          }
      }

      const getChartData = async () => {
        const key = await deviceStorage.getKey('@token')
    
        try {
          const res = await axios.get(
            'https://subsit-team-a.herokuapp.com/api/v1/user/transactions/summary',
            {
              headers: {
                Authorization: key,
              },
            }
          );
    
          if (res !== null) {
            const apiRes = res.data.data;

            console.log('api result chart', apiRes);
              let month = apiRes.monthly_data
              if(month.length>0){
                let obj = {}
                    obj.labels = []
                    obj.datasets = []
                let data = []
                for (let i in month){
                      obj.labels.push(month[i].date)
                      data.push(month[i].total)
                      if(i == month.length-1 ){
                        let datasetsObj = {}
                        datasetsObj.data = data
                        obj.datasets.push(datasetsObj)
                      }
                }
              setchartData(obj)

              }
            
    
    
          }
        } catch (error) {
          console.log('get chart data: ', error);
        }
      };
      
      console.log('chat data',chartData)

      async function customAdd () {
    
        const key = await deviceStorage.getKey('@token')
        
          try {
            await axios.post(
              'https://subsit-team-a.herokuapp.com/api/v1/service/add',
              {
                  title,
                  description,
                  name,
                  repetition,
                  price,
                  card_id,
                  starting_date,
                  
              },
              {
                  headers: {
                      Authorization: key,
                  }
              })
            }catch(error) {
                console.log('get popular: ', error)
              }
      }
      const getCards = async () => {

        const key = await deviceStorage.getKey('@token')
        
        
        try {
          const res = await axios.get(
              'https://subsit-team-a.herokuapp.com/api/v1/user/card',
              {
      
              headers: {
                    Authorization: key,
                  }
              })
          
          if (res !== null) {
              const apiRes = res.data.data.Card;
              setcardsData(apiRes)
              
          }
      }catch(error) {
          console.log('get card: ', error)
        }
      }
      useEffect(() => {
          getCards()
      }, [])

      function CardItem({source, card_number, id, cardSelected}) {
        return(
        <View style={{
          marginHorizontal: 8,
          marginBottom: 5,
          backgroundColor: cardSelected? '#6e3b6e' : '#fff',
          borderRadius: 3,
          }}>
            <TouchableOpacity onPress={()=> {
                setcard_id(id)
                setcardSelected(true)
                }}>
                <Text style={{
                    marginTop: 6,
                    fontSize: 9,
                    color: cardSelected? '#fff' : 'black',
                    }}>{source}</Text>
                <Text style={{
                    marginTop: 3,
                    fontSize: 7,
                    color: cardSelected? '#fff' : 'black',
                }}>{card_number}</Text>
            </TouchableOpacity>
            
        </View>
        )
    }

    return (
      <View style={styles.container}>
            <Background style={styles.background} width={width} height={height}/>
            <Logo size="small"/>
            <View style={{
                flex: 1,
                marginBottom: 30,
            }}
            >
              <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginVertical: 15,}}>
                <Text style={{color: '#fff'}}>Subs: 3</Text>
                <Search type='light' size='small' placeholder='search' bordered/>
              </View>
               {Object.keys(chartData).length !== 0 && <Chart data={chartData}/> } 

            </View>
            <SafeAreaView
              style={{
                flex: 1,
                marginVertical: 15,
                }}
              >
           
            <Text style={styles.title}>Coming Up</Text>
            <Upcoming data={upcomingData}/>
            <Text style={styles.title}>Popular</Text>
            <Popular data={popularData}/>
            <View style={{alignItems: 'center', marginTop: 50,}}>
            <Button text='Add more services' onPress={()=> setshowAddCustom(true)}/>
            </View>
            </SafeAreaView>
            <Modal
              isVisible={showAddCustom}
              onBackdropPress={() => setshowAddCustom(false)}
              animationIn='flipInX'
              useNativeDriver
              onBackButtonPress={() => {
                setshowAddCustom(false)

              }}
              >
                <View style={{
                  height: height / 1.5,
                  width: width/1.12,
                  backgroundColor: '#fff',
                  justifyContent: "space-evenly",
                  paddingHorizontal: 15,
                  borderRadius: 8,
                  }}>
                    <Text style={styles.titleBig}>Add Custom</Text>
                    <InputField placeholder='Service name' onChangeText={(val)=> handleTitle(val)}/>
                    <InputField placeholder='Give some description for the service' onChangeText={(val)=> handleDescription(val)}/>
                    <InputField placeholder='give a nickname' onChangeText={(val)=> handleName(val)}/>
                    <InputField placeholder='Repetition e.g: weekly'onChangeText={(val)=> handleRepetition(val)}/>
                    <InputField placeholder='Price e.g: 10000' onChangeText={(val)=> handlePrice(val)}/>

                    <TouchableOpacity onPress={showDatepicker} style={{paddingVertical: 10, backgroundColor: '#6e3b6e'}}>
                  <Text style={{fontSize: 16, paddingLeft: 5, color: '#fff'}}>Starting at {moment(date).format('DD/MM/YYYY')}</Text>
                  {show && (
                    <DateTimePicker
                      testID="dateTimePicker"
                      value={date}
                      mode={mode}
                      is24Hour={true}
                      display="default"
                      onChange={onChange}
                    />
                  )}
                </TouchableOpacity>

                <View>
                <TouchableOpacity style={{borderColor: '#6e3b6e', borderWidth: 1, borderRadius: 5}}>
                  <Text style={styles.titleBig}>Your payment details</Text>
            
                 <FlatList
                 data={cardsData}
                 showsHorizontalScrollIndicator={false}
                 horizontal
                 renderItem={({ item }) => (
                      <CardItem
                      source={item.source}
                      card_number={item.card_number}
                      id={item.id}
                      cardSelected={cardSelected}
                      keyExtractor={item => item.id}
                      />
                      )}

                 />
                  
                </TouchableOpacity>
              </View>
              <View style={{alignSelf: 'center'}}>
              <Button size='small' bordered text='Submit' onPress={()=> {
                customAdd()
                setshowAddCustom(false)
                ToastAndroid.show('Added', ToastAndroid.SHORT)
              }}/>
              </View>
                  
                </View>

            </Modal>
            
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: "center",
        padding: 20,
    },
    background: {
        position: "absolute",
        top: -50,
        left: 0,
        zIndex: 0,
    },
    title: {
        fontFamily: 'PlayfairDisplay_500Medium',
    },
    tinyLogo: {
        width: 40,
        height: 40,
        borderWidth: 1,
    },
    bigLogo : {
      width: width/1.12,
      height: 120,
      alignSelf: "center",
      borderRadius: 8,
      borderBottomLeftRadius: 0,
      borderBottomRightRadius: 0,
    },
    titleBig : {
      color: 'black',
      fontFamily: 'PlayfairDisplay_700Bold',
      marginVertical: 10,
      paddingLeft: 5, 
      fontSize: 18,
    },
    titleExtraBig : {
      color: 'black',
      fontFamily: 'PlayfairDisplay_700Bold',
      marginVertical: 10,
      paddingLeft: 5, 
      fontSize: 24,
    },
    subTitle : {
      paddingLeft: 5, 
    },
    serviceType: {
      padding: 5,
      margin: 5,
      width: 120,
      flexDirection: 'column',
      borderColor: '#6e3b6e',
      borderRadius: 5,
      borderWidth: 1,
    },
    serviceTypeName: {
      fontSize: 11,
      marginRight: 15,
    }
})
