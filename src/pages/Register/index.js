import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, ToastAndroid } from 'react-native'
import InputField from '../../components/inputField'
import Button from '../../components/button'
import Logo from '../../components/logo'
import deviceStorage from '../../deviceStorage'
import axios from 'axios'

export default function Register({navigation}) {
    const [name, setName] = useState('please change name')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')


    const handleEmail = (val) => {
        setEmail(val)
    }
    
    const handlePassword = (val) => {
        setPassword(val)
    }

    

    const registerApi = async () => {
        try {
            const apiRegister = await axios.post(
                'https://subsit-team-a.herokuapp.com/api/v1/user/register',
                {
                  name,
                  email,
                  password,
                },
              );
              deviceStorage.saveKey("@token", apiRegister.data.data.token)
            
        console.log(name, email, password)
        
        if (apiRegister.data){
            const key = await deviceStorage.getKey('@token')
            console.log(key)
            navigation.replace('Dashboard', {
                name,
                email,
                password,
            });
            
            console.log(email, password)
        }
        }
        catch (error) {
        console.log('register error',error)
        ToastAndroid.show('sorry', ToastAndroid.SHORT)
        }
    }
    return (
        <View style={styles.container}>
            <Logo style={{marginVertical: 3}} type='black' size='large'/>
            <View style={{
                alignContent: "flex-end",
            }}>
                <InputField
                onChangeText={(val)=> handleEmail(val)}
                placeholder='email'

                />
                <InputField
                onChangeText={(val)=> handlePassword(val)}
                placeholder='password'
                secureTextEntry
                />
                <InputField
                onChangeText={(val)=> handlePassword(val)}
                placeholder='confirm password'
                secureTextEntry
                />
                <Button
                text='Sign Up'
                size='large'
                type='black'
                bordered
                onPress={()=> registerApi()}
                />
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                    <Text style={{marginVertical: 10, textAlign: 'right'}}>Sign in</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: "space-around",
        alignSelf: "center",
        padding: 30,
    }
})
