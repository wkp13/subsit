import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, Dimensions, FlatList, TouchableOpacity, TextInput, ToastAndroid } from 'react-native'
import Header from '../../components/pageHeader'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Button from '../../components/button'
import axios from 'axios'
import deviceStorage from '../../deviceStorage'
import Modal from 'react-native-modal'

const {height, width} = Dimensions.get('window');

export default function Profile({navigation}) {
    const [profileData, setProfileData] = useState([])
    const [email, setemail] = useState('')

    const [name, setName] = useState('')
    const [id, setId] = useState('')

    const [editProfile, seteditProfile] = useState(false)
    const [editPassword, seteditPassword] = useState(false)
    const [password, setpassword] = useState('')

    const getProfile = async () => {

        const key = await deviceStorage.getKey('@token')
        
        
        try {
          const res = await axios.get(
              'https://subsit-team-a.herokuapp.com/api/v1/profile/',
              {
      
              headers: {
                    Authorization: key,
                  }
              })
          
          if (res !== null) {
              const apiRes = res.data.data.profile;
              setProfileData(apiRes)
              setName(apiRes.name)
              setId(apiRes.id)
          }
      }catch(error) {
          console.log('get popular: ', error)
        }
      }


      const getUser = async () => {

        const key = await deviceStorage.getKey('@token')
        
        
        try {
          const res = await axios.get(
              'https://subsit-team-a.herokuapp.com/api/v1/user/',
              {
      
              headers: {
                    Authorization: key,
                  }
              })
          
          if (res !== null) {
              const apiRes = res.data.data.user;
              setemail(apiRes.email)
          }
      }catch(error) {
          console.log('get popular: ', error)
        }
      }
      

      useEffect(() => {
          getProfile()
          getUser()
      }, [])

      async function editname () {
    
        const key = await deviceStorage.getKey('@token')
          
          
          try {
            await axios.put(
              'https://subsit-team-a.herokuapp.com/api/v1/profile/update/name',
              {
                  name,
              },
              {
                  headers: {
                      Authorization: key,
                  }
              })
            }catch(error) {
                console.log('get popular: ', error)
              }
      }

      async function actionEditPassword () {
    
        const key = await deviceStorage.getKey('@token')
          
          
          try {
            await axios.put(
              'https://subsit-team-a.herokuapp.com/api/v1/user/update',
              {
                  email,
                  password,
              },
              {
                  headers: {
                      Authorization: key,
                  }
              })
            }catch(error) {
                console.log('get popular: ', error)
              }
      }


    const handleName = (val) => {
    setName(val)
    }

    const handlePassword= (val) => {
        setpassword(val)
    }

    async function logout(){
        await deviceStorage.removeKey('@token')
        navigation.replace('Login')
    }
      

console.log('profile data', profileData)
console.log('email', email)

  return (
            <View
            style={{ 
                flex: 1,
                padding: 20,
                alignItems: 'center',
                justifyContent: "flex-start",
                backgroundColor: '#fff'
                }}
            >
                <Header title='Profile'/>

            <View style={{
                width: width - 40,
                paddingVertical: 10,
                marginTop: 80,
                }}>
                <View style={{justifyContent: 'space-between', flexDirection: 'row', marginBottom: 30,}}>
                    <Text style={styles.title}>{name}</Text>
                </View>
                <Text style={styles.title}>Account</Text>
                <TouchableOpacity
                onPress={()=> seteditProfile(true)}
                style={{
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    borderBottomWidth: 1,
                    paddingVertical: 10,
                    marginBottom:15,
                    }}>
                        <Text style={styles.subTitle}>Change Profile </Text>
                        <MaterialCommunityIcons name='arrow-right' color='black' size={20}/>
                </TouchableOpacity>
                
                <TouchableOpacity onPress={()=> navigation.navigate('Cards')} style={{justifyContent: 'space-between', flexDirection: 'row', borderBottomWidth: 1, paddingVertical: 10, marginBottom:15,}}>
                        <Text style={styles.subTitle}>Cards </Text>
                        <MaterialCommunityIcons name='arrow-right' color='black' size={20}/>
                </TouchableOpacity>

                <Text style={styles.title}>Security</Text>
                <TouchableOpacity
                onPress={()=> seteditPassword(true)}
                style={{
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    borderBottomWidth: 1,
                    paddingVertical: 10,
                    marginBottom:15,
                    }}>
                        <Text style={styles.subTitle}>Change Password </Text>
                        <MaterialCommunityIcons name='arrow-right' color='black' size={20}/>
                </TouchableOpacity>
                
            
                <View style={{marginTop: 40, alignItems: 'center'}}>
                <Button
                text='Sign Out'
                size='large'
                type='filled'
                onPress={()=> logout()}
                />
                </View>

            </View>
            <Modal
            isVisible={editProfile}
            animationIn='flipInX'
            useNativeDriver
            onBackdropPress={() => seteditProfile(false)}
            >
                <View style={{
                    height: height / 4,
                    width: width/1.12,
                    backgroundColor: '#fff',
                    justifyContent: "space-evenly",
                    
                    paddingHorizontal: 15,
                    borderRadius: 8,
                    }}>
                    <Text style={styles.titleExtraBig}>Change Profile</Text>
                    <View style={{
                            borderWidth: 1,
                            padding: 5,
                            borderRadius: 5,
                            borderColor: '#6e3b6e',
                            marginBottom: 20,
                        }}>
                            <TextInput
                            placeholder = 'Edit your name'
                            onChangeText={(val)=> handleName(val)}
                            />
                        </View>
                        <View style={{alignItems: 'center',}}>
                            <Button size='small' bordered text='Submit' onPress={()=>{
                            editname()
                            getProfile()
                            seteditProfile(false)
                            }}/>
                        </View>
                    </View>

            </Modal>
            <Modal
            isVisible={editPassword}
            animationIn='flipInX'
            useNativeDriver
            onBackdropPress={() => seteditPassword(false)}
            >
                <View style={{
                    height: height / 4,
                    width: width/1.12,
                    backgroundColor: '#fff',
                    justifyContent: "space-evenly",
                    
                    paddingHorizontal: 15,
                    borderRadius: 8,
                    }}>
                    <Text style={styles.titleExtraBig}>Change Password</Text>
                    <View style={{
                            borderWidth: 1,
                            padding: 5,
                            borderRadius: 5,
                            borderColor: '#6e3b6e',
                            marginBottom: 20,
                        }}>
                            <TextInput
                            secureTextEntry
                            placeholder = 'Input a new Password'
                            onChangeText={(val)=> handlePassword(val)}
                            />
                        </View>
                        <View style={{alignItems: 'center',}}>
                            <Button size='small' bordered text='Submit' onPress={()=>{
                            actionEditPassword()
                            getProfile()
                            getUser()
                            seteditPassword(false)
                            ToastAndroid.show('Password Updated', ToastAndroid.SHORT)
                            }}/>
                        </View>
                    </View>

            </Modal>

            </View>
  );
}


const styles = StyleSheet.create({
    container: {
        padding: 20,
        flex:1,
        justifyContent: "space-around",
        backgroundColor: '#fff',
    },
    title :{
        fontFamily: 'PlayfairDisplay_700Bold',
        fontSize: 24,
        paddingVertical: 10,
    },
    subTitle: {
        fontSize: 16,
        padding: 5,
    },
    titleExtraBig: {
        fontFamily: 'PlayfairDisplay_700Bold',
        fontSize: 24,
        marginBottom: 30,
    },
})
