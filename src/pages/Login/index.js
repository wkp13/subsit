import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, ToastAndroid } from 'react-native'
import InputField from '../../components/inputField'
import Button from '../../components/button'
import Logo from '../../components/logo'
import deviceStorage from '../../deviceStorage'
import axios from 'axios'

export default function Login({navigation}) {


    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')


    const handleEmail = (val) => {
        setEmail(val)
    }
    
    const handlePassword = (val) => {
        setPassword(val)
    }

    const loginApi = async () => {

        try {
            const apiLogin = await axios.post(
                'https://subsit-team-a.herokuapp.com/api/v1/user/login',
                {
                  email,
                  password,
                },
              )
            deviceStorage.saveKey("@token", apiLogin.data.data.token)

        console.log(email, password)
        
            if (apiLogin.data){

                const key = await deviceStorage.getKey('@token')
                console.log(key)
                navigation.replace('Dashboard', {
                    email,
                    password,
                });
                
                console.log(email, password)
            }
        }
        catch (error) {
        console.log(error)
        ToastAndroid.show('Wrong user name & password. Try again!', ToastAndroid.SHORT)

        }
    }

    return (
        <View style={styles.container}>
            <Logo style={{marginVertical: 3}} type='black' size='large'/>
            <View style={{
                alignContent: "flex-end",
            }}>
                <InputField
                onChangeText={(val)=> handleEmail(val)}
                placeholder='email'

                />
                <InputField
                onChangeText={(val)=> handlePassword(val)}
                placeholder='password'
                secureTextEntry
                />
                <Button
                text='Sign In'
                size='large'
                type='black'
                bordered
                onPress={()=> loginApi()}
                />
                <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                    <Text style={{marginVertical: 10, textAlign: 'right'}}>Register</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                    <Text style={{marginVertical: 10, textAlign: 'right'}}>Forgot password</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: "space-around",
        alignSelf: "center",
        padding: 30,
    }
})
