import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Header from '../../components/pageHeader'


export default function AddSubs({navigation}) {
    return (
        <View style={styles.container}>
            <Header title='Add custom subscription' nameLeft='arrow-left' onPress={()=> navigation.navigate('Dashboard')}/>
            <Text></Text>
        </View>
    )
}

const styles = StyleSheet.create({

})
