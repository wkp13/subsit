import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, FlatList, TouchableOpacity, Dimensions, TextInput } from 'react-native'
import Header from '../../components/pageHeader'
import Visa from '../../assets/visa.svg';
import Button from '../../components/button';
import deviceStorage from '../../deviceStorage'
import axios from 'axios'
import Modal from 'react-native-modal'
import DropDownPicker from 'react-native-dropdown-picker'


const {height, width} = Dimensions.get('window');


export default function Cards({navigation}) {

    const [cardsData, setcardsData] = useState([])
    const [addCard, setaddCard] = useState(false)
    const [source, setsource] = useState('')
    const [card_number, setcardNumber] = useState('')
    const [id, setid] = useState('')
    const [deleteCard, setDeleteCard] = useState(false)

    const handleCardNumber = (val) => {
        setcardNumber(val)
    }
    
    console.log('card id',id)

    async function actionAddCard () {
    
        const key = await deviceStorage.getKey('@token')
          
          
          try {
            await axios.post(
              'https://subsit-team-a.herokuapp.com/api/v1/user/card',
              {
                  source,
                  card_number,
                  
              },
              {
                  headers: {
                      Authorization: key,
                  }
              })
            }catch(error) {
                console.log('get popular: ', error)
              }
      }

      async function actionDeleteCard () {
    
        const key = await deviceStorage.getKey('@token')
          
          
          try {
            await axios.delete(
              `https://subsit-team-a.herokuapp.com/api/v1/user/card/${id}`,
              {
                  headers: {
                      Authorization: key,
                  }
              })
            }catch(error) {
                console.log('get popular: ', error)
              }
      }
    

    console.log('cards data',cardsData)
    const getCards = async () => {

        const key = await deviceStorage.getKey('@token')
        
        
        try {
          const res = await axios.get(
              'https://subsit-team-a.herokuapp.com/api/v1/user/card',
              {
      
              headers: {
                    Authorization: key,
                  }
              })
          
          if (res !== null) {
              const apiRes = res.data.data.Card;
              setcardsData(apiRes)
          }
      }catch(error) {
          console.log('get popular: ', error)
        }
      }
      useEffect(() => {
          getCards()
      })

    function Item({source, card_number, id}) {
        return(
        <View style={{marginVertical: 10,}}>
            <TouchableOpacity onPress={()=> {
                setid(id)
                setDeleteCard(true)
                }}>
                <Text style={{
                    marginTop: 10,
                    fontFamily:'PlayfairDisplay_700Bold',
                    fontSize: 18,
                    }}>{source}</Text>
                <Text style={{
                    marginTop: 10,
                }}>{card_number}</Text>
            </TouchableOpacity>
            
        </View>
        )
    }
    return (
        <View style={styles.container}>
            <Header title='Cards' nameLeft='arrow-left' onPress={()=> navigation.navigate('Profile')}/>
            <View style={{marginTop: 60,}}>
                <FlatList
                data={cardsData}
                renderItem={({item})=> (
                    <Item
                    source={item.source}
                    card_number={item.card_number}
                    id={item.id}
                    keyExtractor={item => item.id}
                    />
                )
                }/>
            </View>
            <View style={{
                justifyContent: 'flex-end',
                alignItems: 'center'
            }}>
                <Button text='Add a new card' onPress={()=> setaddCard(true)}/>
            </View>
            <Modal
                isVisible={addCard}
                onBackdropPress={() => setaddCard(false)}
                animationIn='flipInX'
                useNativeDriver
            >
                <View style={{
                    height: height / 2,
                    width: width/1.12,
                    backgroundColor: '#fff',
                    justifyContent: "center",
                    paddingHorizontal: 15,
                    borderRadius: 8,
                    }}>
                        <Text style={styles.titleExtraBig}>Add a debit card</Text>
                        <Text style={styles.title}>Select Bank</Text>
                        <View style={styles.field}>
                            <DropDownPicker
                                items={[
                                    {
                                        label: 'BCA',
                                        value: 'BCA', selected: true,
                                    },
                                    {
                                        label: 'Mandiri',
                                        value: 'Mandiri'
                                    },
                                    {
                                        label: 'BRI',
                                        value: 'BRI'
                                    },
                                ]}
                                style={{
                                    borderColor: '#6e3b6e',
                                    borderRadius: 5,
                                    padding: 5,
                                }}
                                dropDownStyle={{
                                    borderColor: '#6e3b6e',
                                    borderRadius: 5,
                                    padding: 5,
                                }}
                                defaultIndex={0}
                                containerStyle={{height: 40}}
                                onChangeItem={item => {
                                    console.log(item.label, item.value)
                                    setsource(item.value)
                                }}
                            />
                        </View>
                        
                        <Text style={styles.title}>Input your card number</Text>
                        <View style={{
                            borderWidth: 1,
                            padding: 5,
                            borderRadius: 5,
                            borderColor: '#6e3b6e',
                            marginBottom: 20,
                        }}>
                            <TextInput
                            placeholder = '888900054003999'
                            onChangeText={(val)=> handleCardNumber(val)}/>
                        </View>
                        <View style={{ alignSelf: 'center'}}>
                        <Button size='small' bordered text='Add card' onPress={()=> {
                            actionAddCard()
                            getCards()
                            setaddCard(false)
                            }}/>
                        </View>
                </View>

            </Modal>
            <Modal
            isVisible={deleteCard}
            animationIn='flipInX'
            useNativeDriver
            onBackdropPress={() => setDeleteCard(false)}
            >
                <View style={{
                    height: height / 5,
                    width: width/1.12,
                    backgroundColor: '#fff',
                    justifyContent: "center",
                    alignItems: "center",
                    paddingHorizontal: 15,
                    borderRadius: 8,
                    }}>
                    <Text style={styles.titleExtraBig}>Delete this card?</Text>
                    <Button size='small' bordered text='Yes' onPress={()=>{
                        actionDeleteCard()
                        setDeleteCard(false)
                        getCards()
                    }}/>
                    </View>

            </Modal>
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 20,
        flex:1,
        justifyContent: "space-between",
        backgroundColor: 'white',
    },
    title: {
        fontFamily: 'PlayfairDisplay_700Bold',
        fontSize: 16,
        marginBottom: 15,
    },
    titleExtraBig: {
        fontFamily: 'PlayfairDisplay_700Bold',
        fontSize: 24,
        marginBottom: 30,
    },
    field: {
        marginBottom: 20,
    }
})
