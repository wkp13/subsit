import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, FlatList, Dimensions} from 'react-native'
import axios from 'axios'
import deviceStorage from '../../deviceStorage'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Modal from 'react-native-modal'
import Button from '../../components/button'
import Header from '../../components/pageHeader'

const {height, width} = Dimensions.get('window');


export default function Report() {

const [servicesData, setservicesData] = useState([])
const [unsubscribe, setunsubscribe] =useState(false)
const [id, setid] = useState('')

    const getAllServices = async () => {

        const key = await deviceStorage.getKey('@token')
        
        try {
          const res = await axios.get(
              'https://subsit-team-a.herokuapp.com/api/v1/service/user',
              {
      
              headers: {
                    Authorization: key,
                  }
              })
          
          if (res !== null) {
              const apiRes = res.data.data;
              setservicesData(apiRes)
          }
      }catch(error) {
          console.log('get services: ', error)
        }
      }
  
      useEffect(() => {
        getAllServices()
      })
  
      console.log('get all', servicesData)

      function Item({id, title, description}){
          return(
              <View style={{
                  borderWidth: 1,
                  borderColor: '#6e3b6e',
                  borderRadius: 10,
                  padding: 5,
                  marginVertical: 10,
              }}>
                  <TouchableOpacity onPress={()=> {
                      setunsubscribe(true)
                      setid(id)
                      }}>
                    <Text style={styles.titleBig}>{title}</Text>
                    <Text style={styles.subTitle}>{description}</Text>
                  </TouchableOpacity>
              </View>
          )
      }
      async function actionUnsubscribe(){

        const key = await deviceStorage.getKey('@token')
          
          
          try {
            await axios.delete(
              `https://subsit-team-a.herokuapp.com/api​/v1​/service​/${id}`,
              {
                  headers: {
                      Authorization: key,
                  }
              })
            }catch(error) {
                console.log('get popular: ', error)
              }
      }

    return (
        <View style={styles.container}>
          <Header title='Report'/>
            <View style={{
                padding: 10,
                marginVertical: 10,
                marginTop: 60,
                borderRadius: 10,
                borderWidth: 1,
                borderColor: '#6e3b6e'
            }}>
                <View style={{justifyContent: 'center'}}>
                    <Text style={{fontFamily: 'PlayfairDisplay_700Bold', fontSize: 30, marginBottom: 10,}}>SubsIt. </Text>
                    <Text style={{fontSize: 18,}}>Wallet</Text>
                </View>
                <View>
                    <Text>Balance : </Text>
                    <Text>Last transaction: </Text>
                </View>
            </View>
            <View>
                <Text style={{
                    fontSize: 20,
                    fontFamily: 'PlayfairDisplay_700Bold'
                }}>Your Services</Text>
                <FlatList
                data={servicesData}
                renderItem={({item})=> (
                    <Item
                    title={item.title}
                    description={item.description}
                    id={item.id}
                    keyExtractor={item => item.id}
                    />
                )
                }/>
            </View>
            <Modal
            isVisible={unsubscribe}
            animationIn='flipInX'
            useNativeDriver
            onBackdropPress={() => setunsubscribe(false)}
            >
                <View style={{
                    height: height / 5,
                    width: width/1.12,
                    backgroundColor: '#fff',
                    justifyContent: "center",
                    alignItems: "center",
                    paddingHorizontal: 15,
                    borderRadius: 8,
                    }}>
                    <Text style={styles.titleExtraBig}>Unsubscribe this service?</Text>
                    <Button size='small' bordered text='Yes' onPress={()=>{
                        actionUnsubscribe()
                        setunsubscribe(false)
                        getAllServices()
                    }}/>
                    </View>

            </Modal>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        padding: 30,
        backgroundColor: '#fff',
    },
    title : {
        color: 'black',
        fontFamily: 'PlayfairDisplay_500Medium',
        marginHorizontal: 10,
      },
      tinyLogo: {
          width: 40,
          height: 40,
          borderWidth: 1,
      },
      bigLogo : {
        width: width/1.12,
        height: 120,
        alignSelf: "center",
        borderRadius: 8,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
      },
      titleBig : {
        color: 'black',
        fontFamily: 'PlayfairDisplay_700Bold',
        marginVertical: 10,
        paddingLeft: 5, 
        fontSize: 18,
      },
      titleExtraBig : {
        color: 'black',
        fontFamily: 'PlayfairDisplay_700Bold',
        marginVertical: 10,
        paddingLeft: 5, 
        fontSize: 24,
      },
      subTitle : {
        paddingLeft: 5, 
      },
      serviceType: {
        padding: 5,
        margin: 5,
        width: 120,
        flexDirection: 'column',
        borderColor: '#6e3b6e',
        borderRadius: 5,
        borderWidth: 1,
      },
      serviceTypeName: {
        fontSize: 11,
        marginRight: 15,
      }
})
