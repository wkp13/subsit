import React, { useState, useEffect } from 'react';
import { Text, View } from 'react-native';

export default function Splash ({navigation}) {

    setTimeout(() => {
       navigation.replace('Login') 
    }, 1000);
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text
          style={{ 
           fontSize: 60,
           fontFamily: 'PlayfairDisplay_700Bold',
          }}>
          SubsIt.
        </Text>
      </View>
    );
}